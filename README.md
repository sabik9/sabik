# Ansible LAMP  Playbook


### Stack -
* apache2
* php7.1-fpm 

### Pre-requisites
You must have Ansible 2.3 or later installed.

### Setup
* Edit host in hosts file.
* Run command `sudo ansible-playbook -i hosts lamp-playbook.yml`







